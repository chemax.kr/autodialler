CREATE TABLE IF NOT EXISTS `jobs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `cron` varchar(1000) NOT NULL DEFAULT '* * * * *',
  `trunk` varchar(100) NOT NULL,
  `sound` varchar(100) NOT NULL,
  `number` varchar(100) NOT NULL,
  `comment` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;
