<?php

namespace Project\App\HTTP;

use PHPixie\HTTP\Request;

/**
 * Simple greeting web page
 */
class Main extends Processor
{
    /**
     * Default action
     * @param Request $request HTTP request
     * @return mixed
     */
    public function defaultAction($request)
    {
        $template = $this->components()->template();

        $container = $template->get('app:greet');
        $container->message = "Have fun coding!";
        return $container;
    }

    public function joblistAction()
    {
        $template = $this->components()->template();

        $container = $template->get('app:joblist');
        $container->jobList = $this->components()->orm()->query('job')->find();
        return $container;
    }

    public function editJobAction($request)
    {
        $template = $this->components()->template();

        $container = $template->get('app:jobedit');
        $container->id = $request->attributes()->get('id');
        if($request->attributes()->get('id'))
        {
            $container->jobParam = $this->components()->orm()->query('job')->in($request->attributes()->get('id'))->findOne();
        }
        //$container->jobList = $this->components()->orm()->query('job')->find();
        return $container;
    }

    protected function saveJob($job)
    {
        if($job['id'])
        {
            $jobE = $this->components()->orm()->query('job')->in($job['id']);
            $jobE->update($job);
            return 'ok';

        }
        else
        {
            $jobE = $this->components()->orm()->repository('job');
//            var_dump($job);
            $jobE->create($job)->save();
            return 'ok';
        }


    }

    public function startAction()
    {
        $this->stopAction();
        $nodeBin = exec("which node");
        $command = $nodeBin." ".__DIR__."/../../../../bin/autodialler/dialer.js &";
        exec($command);
        return $this->redirect('app.action', ['processor' => 'main', 'action' => 'default', 'id' => NULL]);
    }

    public function stopAction()
    {
        exec("pkill node");
        return $this->redirect('app.action', ['processor' => 'main', 'action' => 'default', 'id' => NULL]);
    }

    public function deleteJobAction($request)
    {
        $id = $request->attributes()->get('id');
        $this->components()->orm()->query('job')->in($id)->delete();
        return $this->redirect('app.action', ['processor' => 'main', 'action' => 'joblist', 'id' => NULL]);
    }

    public function saveJobAction($request)
    {
        $job = [
            'id' => $request->data()->get('id'),
            'name' => $request->data()->get('name'),
            'cron' => $request->data()->get('cron'),
            'trunk' => $request->data()->get('trunk'),
            'number' => $request->data()->get('number'),
            'sound' => $request->data()->get('sound'),
            'comment' => $request->data()->get('comment'),
        ];

        $this->saveJob($job);
        return $this->redirect('app.action', ['processor' => 'main', 'action' => 'joblist', 'id' => NULL]);

    }


    public function sipconfAction($request)
    {
        $file = $request->attributes()->get('id');
        $template = $this->components()->template();
        $container = $template->get('app:sipConf');
        $container->file = $file;
        $container->sipConf = file_get_contents('/etc/asterisk/'.$file);
        return $container;
    }

    protected function asteriskReload()
    {
        exec('sudo /usr/sbin/service asterisk restart');
    }




    public function saveSipConfAction($request)
    {

        $file = $request->data()->get('file');
        $fileContent = $request->data()->get('sipConf');
        file_put_contents('/etc/asterisk/'.$file,$fileContent);
        return $this->redirect('app.action', [
            'processor' => 'main',
            'action' => 'sipconf',
            'id' => $file
        ]);
    }

}