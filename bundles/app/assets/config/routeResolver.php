<?php

return array(
    'type'      => 'group',
    'defaults'  => array('action' => 'default'),
    'resolvers' => array(
        
        'action' => array(
            'path' => '<processor>/<action>/(<id>)',
            'defaults' => array('id' => NULL)
        ),

        'processor' => array(
            'path'     => '(<processor>)',
            'defaults' => array('processor' => 'main')
        )
    )
);
