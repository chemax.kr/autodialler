<?php
/**
 * Created by PhpStorm.
 * User: chemax
 * Date: 09.07.2017
 * Time: 17:11
 */
if (!$id) {

    $name = '';
    $cron = '* * * * *';
    $trunk = '';
    $number = '';
    $comment = '';
    $sound = '';

} else {
    $name = $jobParam->name;
    $cron = $jobParam->cron;
    $trunk = $jobParam->trunk;
    $number = $jobParam->number;
    $comment = $jobParam->comment;
    $sound = $jobParam->sound;
}

?>
<?php $this->layout('app:layout'); ?>
<div>
    <form action="/main/saveJob/" method="post">
        <input type="text" id="id" name="id" value="<?= $id ?>" hidden>

        <p><label>Имя задания: </label><input type="text" id="name" name="name" required value="<?= $name ?>">
        </p>
        <p><label>Расписание: </label><input type="text" id="cron" name="cron" required value="<?= $cron ?>">
        </p>
        <p><label>Имя транка: </label><input type="text" id="trunk" name="trunk" required value="<?= $trunk ?>">
        </p>
        <p><label>Куда звонить: </label><input type="text" id="number" name="number" required value="<?= $number ?>">
        </p>
        <p><label>Имя звукового файла: </label><input type="text" id="sound" name="sound" value="<?= $sound ?>">
        <p>Складывать файлы сюда: /var/www/web-dialer/sounds/

        </p>
        </p>
        <p><label>Комментарий: </label><input type="text" id="comment" name="comment" value="<?= $comment ?>">
        </p>
        <p><input type="submit" value="отправить"></p>
        <p><a href="/main/joblist/">Назад</a></p>
    </form>
</div>

<div>
    <h2>Небольшая памятка по расписанию</h2>
    <h3>Очевидно, что инструкция с крона линукса, так что упоминание команд в инструкции стоит игнорировать. Вместо них у нас будут совершаться звонки.</h3>
    <h4>В отличии от линуксового крона этот умеет в секунды, это необязательный первый параметр.</h4>
    <p>
        Если параметров 5, то секунды игнорируются, если 6 то первый параметр это не минуты а секунды, остальное сдвигается.
    </p>
    <p><a href="https://www.npmjs.com/package/node-cron">Оригинальная инструкция модуля расписаний</a></p>
    <pre>------------------------------------------------
минута час день_месяца месяц день_недели команда
------------------------------------------------

Допустимые значения:
минута        от 0 до 59
час           от 0 до 23
день_месяца   от 1 до 31
месяц         от 1 до 12 (можно три буквы из названия месяца,
                          регистр не имеет значения от jan до dec)
день_недели   от 0 до 6  (0 это воскресенье,
                          можно писать от sun до sat)
</pre>
    <p>Каждое из полей даты и времени может быть обозначено символом * ,будет соответствовать любому возможному
        значению. Для этих полей можно указывать диапазоны значений, разделенных дефисом,
        например:
    </p>
    <pre>* 5 4-10 0-3 * echo "HELLO"    -печать HELLO в 5:00 на 4,5,6,7,8,9,10
                                дни января, февраля, марта и апреля
</pre>
    <pre>* */2 * * sat echo "HELLO"     -печать HELLO каждый четный час,
                                каждую субботу
</pre>
    <p>равнозначная предыдущему примеру запись (списком)
    </p>
    <pre>* 0,2,4,6,8,10,12,14,16,18,20,22 * * sat echo "HELLO"
                               -печать HELLO каждый четный
                                час, каждую субботу
</pre>
    <p>то же самое с указанием диапазона
    </p>
    <pre>* 0-23/2 * * sat echo "HELLO"  -печать HELLO каждый четный
                                час, каждую субботу

59 23 31 dec * echo "Happy new year" -без комментариев :),
                                      поздравит с новым годом
</pre>
</div>
