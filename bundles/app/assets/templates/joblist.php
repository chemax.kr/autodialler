<?php
/**
 * Created by PhpStorm.
 * User: chemax
 * Date: 09.07.2017
 * Time: 16:58
 */
?>
<?php $this->layout('app:layout'); ?>
<a href="/main/editJob/">Создать задачу</a>
<div>
    <ul>
        <?php foreach ($jobList as $job): ?>
            <li>
                <?= $job->name ?>
                <a href="/main/editJob/<?= $job->id ?>">
                    [редактировать]
                </a>
                <a href="/main/deleteJob/<?= $job->id ?>">
                    [копировать]
                </a>
                <a href="/main/deleteJob/<?= $job->id ?>">
                    [Удалить]
                </a>
                <p>
                    Расписание:
                    <?= $job->cron ?>
                </p>
                <p>
                    Комментарий:
                    <?= $job->comment ?>
                </p>
            </li>
        <?php endforeach; ?>
    </ul>
</div>
